package com.devcamp.customerlnvoiceapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customerlnvoiceapi.model.Invoice;

@Service
public class InvoiceService {

    @Autowired
    private CustomerService customerService;
    // Khởi tạo 3 đối tượng hóa đơn
    public ArrayList<Invoice> getInvoiceList() {
    Invoice invoice1 = new Invoice(1, customerService.khach_hang1, 120000);
    Invoice invoice2 = new Invoice(2, customerService.khach_hang2, 150000);
    Invoice invoice3 = new Invoice(3, customerService.khach_hang3, 170000);
    ArrayList<Invoice> vArrInvoiceList = new ArrayList<Invoice>();
    vArrInvoiceList.add(invoice1);
    vArrInvoiceList.add(invoice2);
    vArrInvoiceList.add(invoice3);
        return vArrInvoiceList;
    }

    //đầu ra Invoice thực hiện trả ra Invoice thứ index trong danh sách invoice đã khai báo
    public Invoice getInvoiceIndex(int index) {
        ArrayList<Invoice> allInvoice = getInvoiceList();
        return allInvoice.get(index);
    }

}
