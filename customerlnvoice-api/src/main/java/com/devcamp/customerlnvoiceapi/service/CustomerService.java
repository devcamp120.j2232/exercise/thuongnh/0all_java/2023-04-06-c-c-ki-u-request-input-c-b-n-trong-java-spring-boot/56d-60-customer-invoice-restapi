package com.devcamp.customerlnvoiceapi.service;
import java.util.ArrayList;
import org.springframework.stereotype.Service;
import com.devcamp.customerlnvoiceapi.model.Customer;

@Service
public class CustomerService {
     
     Customer  khach_hang1 = new Customer(10226, "thuong", 20);
     Customer  khach_hang2 = new Customer(10227, "duong", 50);
     Customer  khach_hang3 = new Customer(10228, "linh", 70);

     public ArrayList <Customer> getCustomerList() {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        customers.add(khach_hang1);
        customers.add(khach_hang2);
        customers.add(khach_hang3);
        return customers;
     }
    
}
