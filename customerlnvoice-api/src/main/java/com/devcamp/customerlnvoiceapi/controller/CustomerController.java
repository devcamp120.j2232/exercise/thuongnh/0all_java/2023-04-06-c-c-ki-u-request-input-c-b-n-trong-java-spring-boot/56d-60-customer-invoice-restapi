package com.devcamp.customerlnvoiceapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerlnvoiceapi.model.Customer;
import com.devcamp.customerlnvoiceapi.service.CustomerService;

@CrossOrigin   //Java @CrossOrigin: cho phép CORS trên RESTful web service.
@RestController
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    @GetMapping("/customers")
    public ArrayList<Customer> getArrList(){
        ArrayList<Customer> customers = customerService.getCustomerList();
        return customers;

    }
}
