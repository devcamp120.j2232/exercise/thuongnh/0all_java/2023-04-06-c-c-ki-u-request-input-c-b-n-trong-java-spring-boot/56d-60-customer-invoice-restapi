package com.devcamp.customerlnvoiceapi.controller;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerlnvoiceapi.model.Invoice;
import com.devcamp.customerlnvoiceapi.service.InvoiceService;

@CrossOrigin   //Java @CrossOrigin: cho phép CORS trên RESTful web service.
@RestController
public class InvoiceController {
    @Autowired
    private InvoiceService invoiceService;

    @GetMapping("/invoices")
    public ArrayList<Invoice> getArrInvoices() {
        ArrayList<Invoice> invoices = invoiceService.getInvoiceList();
        return invoices;
    } 

    @GetMapping("/invoices/{invoiceIndex}")
    public Invoice getInvoicesIndex(@PathVariable (name="invoiceIndex") int invoiceIndex) {
        Invoice invoices = invoiceService.getInvoiceIndex(invoiceIndex);
        return invoices;
    } 
    
}
